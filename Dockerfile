FROM nginx:stable-alpine
WORKDIR /usr/share/nginx/html

ADD ./dist/games-you-own-frontend/ ./

RUN rm /etc/nginx/conf.d/default.conf
ADD nginx.conf /etc/nginx/conf.d
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
