# Games You Own Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.
## Running in dev environment

Run `npm install` followed by `npm start` for a start a dev server. Navigate to `http://localhost:4200/` to see the app. In development, the app will call the backend on `http://localhost:5000/`

You also can run in local using docker:
1. Change the `backendHost` property in `environment.prod.ts` to `http://localhost:5000`
2. run `npm install && npm run build`
2. run `docker-compose up -d`

Make sure you don't have any app running in port 5001.

## About the assignment

I make the following list of frontend / angular skills that I applied to develop this app. Consider to check it:
- Use of Angular 11 and Angular Material with custom theme;
- Strict mode and strict templates enabled;
- Use of Reactive Forms and custom validations in game form;
- Created the custom component (selector game) that works in angular native forms and make async requests to backend: `src/app/game-form/select-game`;
- Creation of a service module, to control features like: API call, message handling and global loading;
- Use of CSS `flex` properties to help making the game tiles in home page responsive in small screens;
- Use of RxJS in search bar and selector game components;
- Use of NgRx to handling the state for SearchBar data: `src/app/home/search-bar` and `src/app/home/state-manager`;
- The app was dockerized and I also created a pipeline in jenkins to automatically build and deploy the app in production when a pull request or a merge is made in master branch;

This app uses the [Rawg.io API](https://rawg.io/apidocs) for fetching gaming images. The calls for fetching information of games was wraped in the backend app. AOT compiling is running by default when building in Angular 11.

## Points that I didn't and how I would do it
- A login page with access control;
- Caching control for the game images and work with low quality images to better performance of home page;
- Create and run E2E tests with Testcafe after the deployment;







