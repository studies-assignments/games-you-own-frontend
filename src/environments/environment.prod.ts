export const environment =  {
  production: true,
  backendHost: "https://games-you-won.jsouza.dev/api",
  appName: 'Games You Own',
  backendErrorChance: 0
};
