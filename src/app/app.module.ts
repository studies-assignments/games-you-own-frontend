import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './core/header/header.component';
import { LoadingComponent } from './core/loading/loading.component';
import { PageComponent } from './core/page/page.component';
import { GameFormModule } from './game-form/game-form.module';
import { HomeModule } from './home/home.module';




@NgModule({
  declarations: [PageComponent, HeaderComponent, LoadingComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    AppRoutingModule,
    HomeModule,
    GameFormModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [PageComponent]
})
export class AppModule { }
