import { Injectable } from "@angular/core";
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';


@Injectable()
export class MessageService {

  private defaultConfigs : MatSnackBarConfig = {
    duration: 5000
  };

  constructor(private snackBar : MatSnackBar) {}

  showMessage(message : string) {
    this.snackBar.open(message, "Close", this.defaultConfigs)
  }

  showErrorMessage(message : string) {
    this.snackBar.open(`Error: ${message}`, "Close", this.defaultConfigs)
  }

}