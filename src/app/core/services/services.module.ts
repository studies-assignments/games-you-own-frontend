import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameService } from './game-service/game-service';
import { LoadingService } from './loading-service/loading-service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MessageService } from './message-service/message-service';




@NgModule({
  providers: [GameService, LoadingService, MessageService],
  imports: [
    CommonModule,
    MatSnackBarModule
  ]
})
export class ServicesModule { }
