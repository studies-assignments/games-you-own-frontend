import { Injectable } from "@angular/core";
import { GameEntityInterface } from "../../domain/gameEntity";
import { environment } from "../../../../environments/environment";
import { LoadingService } from "../loading-service/loading-service";

@Injectable()
export class GameService {

  private defaultHeaders = { 'Accept': 'application/json', 'Content-Type': 'application/json' };

  constructor(private loadingService : LoadingService) {}

  save(game: GameEntityInterface): Promise<GameEntityInterface> {
    if (this.shouldThrowError()) {
      return Promise.reject("Random Error");
    }

    this.loadingService.enable();
    return fetch(`${environment.backendHost}/games`, {
      method: "POST",
      body: JSON.stringify(game),
      headers: this.defaultHeaders,
    }).then(d => d.json())
      .finally(() => this.loadingService.disable());
  }

  list(name = "", console = "") : Promise<GameEntityInterface[]> {
    this.loadingService.enable();
    return fetch(`${environment.backendHost}/games?name=${name}&console=${console}`, {
      method: "GET",
      headers: this.defaultHeaders,
    }).then(d => d.json())
      .finally(() => this.loadingService.disable());
  }

  get(id : string) : Promise<GameEntityInterface> {
    this.loadingService.enable();
    return fetch(`${environment.backendHost}/games/${id}`, {
      method: "GET",
      headers: this.defaultHeaders,
    }).then(d => d.json())
      .finally(() => this.loadingService.disable());
  }
  
  delete(id : string) : Promise<any> {
    this.loadingService.enable();
    return fetch(`${environment.backendHost}/games/${id}`, {
      method: "DELETE",
      headers: this.defaultHeaders,
    }).finally(() => this.loadingService.disable());
  }

  search(name : string) {
    return fetch(`${environment.backendHost}/games/all?name=${name}`, {
      method: "GET",
      headers: this.defaultHeaders,
    }).then(d => d.json())
  }

  private shouldThrowError() : boolean {
    return Math.random() < (environment.backendErrorChance ?? 0)
  }

}