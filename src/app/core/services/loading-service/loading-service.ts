import { Injectable } from "@angular/core";
import { BehaviorSubject, NextObserver } from "rxjs";

@Injectable()
export class LoadingService {

  private loadingObserver = new BehaviorSubject(false);

  enable() {
    this.loadingObserver.next(true);
  }

  disable() {
    this.loadingObserver.next(false)
  }

  subscribe(observer : NextObserver<boolean>) {
    return this.loadingObserver.subscribe(observer);
  }

  value() {
    return this.loadingObserver.getValue();
  }

}