import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LoadingService } from '../services/loading-service/loading-service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  hide : boolean = false;

  constructor(private loadingService : LoadingService) { }

  ngOnInit(): void {
    this.loadingService.subscribe({ next: this.setHide.bind(this) })
  }

  private setHide(newValue : boolean) {
    this.hide = newValue;
  } 

}
