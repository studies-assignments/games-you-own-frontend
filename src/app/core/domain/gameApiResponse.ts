import { GameInfo } from "./gameEntity";
import { Platforms } from "./platforms";

export interface GameApiResponse {
  id: string,
  background_image: string,
  name: string,
  platforms: {platform: {name: string}}[],
  released: string
}

export const convertToGameInfo = (apiResponse : GameApiResponse) : GameInfo => {
  const platformsNames = Object.values(Platforms).map(p => p.text.toLowerCase());

  return {
    apiId: apiResponse.id,
    name: apiResponse.name,
    imageUrl: apiResponse.background_image,
    releasedYear: new Date(apiResponse.released).getFullYear(),
    console: apiResponse.platforms?.map(p => p.platform.name.toLowerCase())
      .filter(p => platformsNames.includes(p))
      .map(p => Object.values(Platforms).filter(v => v.text.toLowerCase() === p.toLowerCase()).map(v => v.name)[0])
  }
}