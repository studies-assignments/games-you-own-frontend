export const Platforms = {
  PS4: {name: "PS4", text: "Playstation 4"},
  PS5: {name: "PS5", text: "Playstation 5"},
  XBOX_ONE: {name: "XBOX_ONE", text: "Xbox One"},
  NINTENDO_SWITCH: {name: "NINTENDO_SWITCH", text: "Nintendo Switch"},
  PC: {name: "PC", text: "PC"},
}

export const findPlatformByName = (name : string) => {
  return Object.entries(Platforms).filter(([k, v]) => k === name).map(([k, v]) => v)[0];
}