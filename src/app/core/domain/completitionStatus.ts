export const CompletitionStatus = {
  NOT_COMPLETED: { name: "NOT_COMPLETED", text: "Not Started" },
  IN_PROGRESS: { name: "IN_PROGRESS", text: "In Progress" },
  COMPLETED: { name: "COMPLETED", text: "Completed" },
}

export interface Option {
  name: string,
  text: string
}