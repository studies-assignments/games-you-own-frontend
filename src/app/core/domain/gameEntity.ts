import { CompletitionStatus } from "./completitionStatus";

export const GameEntity : GameEntityInterface = {
  id: "",
  completitionStatus: CompletitionStatus.NOT_COMPLETED.name,
  personalNotes: "",
  completitionDate : "",
  info: null
}

export interface GameEntityInterface {
  id: string,
  completitionStatus: string,
  personalNotes: string,
  completitionDate: string,
  info : GameInfo | null
}

export interface GameInfo {
  apiId: string,
  name : string,
  releasedYear : number,
  console: string[],
  imageUrl: string
}