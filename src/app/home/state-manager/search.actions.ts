import { createAction, props } from '@ngrx/store';
import { SearchBarData } from '../search-bar/search-bar.component';

export interface SearchAction {
  searchData : SearchBarData
}
export const search = createAction('[Search Component] Search', props<SearchAction>());