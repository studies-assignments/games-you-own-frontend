import { Action, createReducer, on } from '@ngrx/store';
import { SearchBarData } from '../search-bar/search-bar.component';
import { search, SearchAction } from './search.actions';
 
export const initialState : SearchBarData = { console : "", input: "" };
 
const _searchReducer = createReducer(
  initialState,
  on(search, (state, action : SearchAction) => {
    return action.searchData;
  })
);
 
export function searchReducer(state : SearchBarData | undefined, action : Action) {
  return _searchReducer(state, action);
}