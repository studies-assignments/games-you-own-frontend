import { Component, forwardRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, filter, take } from 'rxjs/operators';
import { Platforms } from 'src/app/core/domain/platforms';
import { search } from '../state-manager/search.actions';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SearchBarComponent), multi: true
  }]
})
export class SearchBarComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  platforms = Object.values(Platforms);
  search$: Observable<SearchBarData>;

  constructor(private formBuilder: FormBuilder,
    private store: Store<{ search: SearchBarData }>) {
    this.search$ = store.select('search');
  }

  async ngOnInit(): Promise<void> {
    this.form = this.formBuilder.group({ input: "", console: "" });
    this.store.dispatch(search({ searchData: { ...this.form.value } }));
    this.setOnChanges();
  }

  private setOnChanges() {
    this.form.controls.input?.valueChanges.pipe(
      debounceTime(300), // Request only with 300ms of delay
    ).subscribe((input: string) => {
      this.store.dispatch(search({ searchData: { ...this.form.value, input } }));
    })

    this.form.controls.console?.valueChanges.subscribe((console: string) => {
      this.store.dispatch(search({ searchData: { ...this.form.value, console } }));
    });
  }


  clearInput() {
    this.form.controls.input?.setValue("");
  }

}

export interface SearchBarData {
  input: string,
  console: string
}
