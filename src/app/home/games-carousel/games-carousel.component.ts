import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CompletitionStatus } from 'src/app/core/domain/completitionStatus';
import { GameEntityInterface } from 'src/app/core/domain/gameEntity';
import { Platforms } from 'src/app/core/domain/platforms';
import { GameService } from 'src/app/core/services/game-service/game-service';
import { SearchBarData } from '../search-bar/search-bar.component';

@Component({
  selector: 'app-games-carousel',
  templateUrl: './games-carousel.component.html',
  styleUrls: ['./games-carousel.component.scss']
})
export class GamesCarouselComponent implements OnInit {

  games : GameEntityInterface[] = [];
  filteredGames : GameEntityInterface[] = [];
  search$ : Observable<SearchBarData>;

  constructor(private gameService : GameService, store: Store<{ search: SearchBarData }>) {
      this.search$ = store.select('search');
  }

  async ngOnInit(): Promise<void> {
    const games = await this.gameService.list();
    this.games = games;
    this.search$.subscribe(this.filterGames.bind(this))
  }

  private filterGames(searchData : SearchBarData) {
    const { input = "", console : platform = "" } = searchData ?? {};
    this.filteredGames = this.games.filter(game => {
      const inputRule = game.info?.name.toLowerCase().includes(input.toLowerCase())
      const consoleRule = platform === "" || game.info?.console.includes(platform);
      return inputRule && consoleRule;
    })
  }

  getCompletitionName(status : string) : string {
    return Object.entries(CompletitionStatus).filter(([k, v]) => k === status).map(([k, v]) => v.text)[0];
  }

  getPlatformNames(platforms : string[] | undefined) : string {
    return platforms?.map(p => {
      return Object.entries(Platforms).filter(([k, v]) => k === p).map(([k, v]) => v.text)[0] ?? ""
    }).join(", ") ?? ""
  }

}
