import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamesCarouselComponent } from './games-carousel/games-carousel.component';
import { HomeRoutingModule } from './home-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HomeComponent } from './home.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { StoreModule } from '@ngrx/store';
import { searchReducer } from './state-manager/search.reducer';

@NgModule({
  declarations: [GamesCarouselComponent, HomeComponent, SearchBarComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    StoreModule.forRoot({ search : searchReducer })
  ],
})
export class HomeModule { }
