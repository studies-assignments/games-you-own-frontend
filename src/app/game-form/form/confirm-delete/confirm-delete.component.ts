import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { GameEntityInterface } from "src/app/core/domain/gameEntity";

@Component({
  selector: 'confirm-delete',
  templateUrl: 'confirm-delete.component.html',
})
export class ConfirmDeleteComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: GameEntityInterface) {
  }
}