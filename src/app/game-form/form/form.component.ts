import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CompletitionStatus } from 'src/app/core/domain/completitionStatus';
import { GameEntityInterface } from 'src/app/core/domain/gameEntity';
import { GameService } from 'src/app/core/services/game-service/game-service';
import { MessageService } from 'src/app/core/services/message-service/message-service';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form: FormGroup = new FormGroup({})

  completitionStatusList = Object.values(CompletitionStatus);

  constructor(private formBuilder: FormBuilder,
    private gameService: GameService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: "",
      completitionStatus: new FormControl(CompletitionStatus.NOT_COMPLETED.name, [Validators.required]),
      personalNotes: new FormControl("", [Validators.required, Validators.maxLength(200)]),
      completitionDate: new FormControl("", [this.completitionDateValidator.bind(this)]),
      info: new FormControl(null, [this.gameSelectedValidator.bind(this)])
    });
    this.setOnChanges();
    this.loadEntityIfExists();

  }

  onSubmit(): void {
    if (this.form.invalid) {
      this.messageService.showErrorMessage("Complete the requirements of all fields before submit");
      return;
    }
    this.gameService.save(this.form.value)
      .then((data: GameEntityInterface) => {
        this.messageService.showMessage(`${data.info?.name} was successfully saved!`);
        this.router.navigateByUrl("/")
      })
      .catch(error => {
        console.error(`Failed when trying to save game: ` + error);
        this.messageService.showErrorMessage('Failed when trying saving, try again later');
      });
  }

  delete(event: Event): void {
    event.preventDefault();
    this.dialog.open(ConfirmDeleteComponent, { data: this.form.value }).afterClosed().subscribe(ok => {
      if (ok) {
        this.gameService.delete(this.form.value.id).then(() => {
          this.messageService.showMessage(`${this.form.value.info?.name} was successfully removed!`);
          this.router.navigateByUrl("/")
        }).catch(error => {
          console.error(`Failed when tried to remove the game: ` + error);
          this.messageService.showErrorMessage('Failed when trying removing, try again later');
        });
      }
    })
  }

  getErrors(errors: ValidationErrors | null | undefined) {
    return Object.values(errors ?? {}).join(', ');
  }

  private setOnChanges() {
    this.form.get('completitionStatus')?.valueChanges.subscribe(this.toggleDisableCompletitionDate.bind(this));
    this.toggleDisableCompletitionDate(this.form.get('completitionStatus')?.value);
  }

  private toggleDisableCompletitionDate(completitionStatus: string) {
    const completitionDate = this.form.get('completitionDate');
    if (completitionStatus === CompletitionStatus.COMPLETED.name) {
      completitionDate?.enable();
      if (completitionDate?.pristine && !completitionDate.value) {
        completitionDate?.setValue(new Date().toISOString());
      }
    } else {
      completitionDate?.setValue("");
      completitionDate?.disable();
    }
  }

  private loadEntityIfExists() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.gameService.get(id).then((data: GameEntityInterface) => {
        this.form.patchValue(data);
      }).catch(error => {
        console.error(`Failed when trying to get the game: ` + error);
        this.messageService.showErrorMessage('Failed when trying to get the game, try again later');
      })
    }
  }

  private completitionDateValidator(control: AbstractControl): { [key: string]: any } | null {
    let errorKey = null;

    if (control.value === "" && this.form.get('completitionStatus')?.value === CompletitionStatus.COMPLETED.name) {
      errorKey = { completitionDateRequired: "Completition date is required for completed games" };
    }

    if (new Date(control.value).getTime() > new Date().getTime()) {
      errorKey = { completitionDateFuture: "Completition date can't be in future" };
    }

    if (new Date(control.value).getTime() < new Date(1970, 1, 1).getTime()) {
      errorKey = { completitionDatePast: "Campletition date can't be before than 1970" };
    }

    return errorKey;
  }

  private gameSelectedValidator(control: AbstractControl): { [key: string]: any } | null {
    if (control.value?.apiId == null) {
      return { gameInfoRequired: "Please select a game" }
    }
    return null;
  }

}