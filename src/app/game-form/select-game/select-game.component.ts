import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { from } from 'rxjs';
import { debounceTime, filter, finalize, switchMap, tap } from 'rxjs/operators';
import { convertToGameInfo, GameApiResponse } from 'src/app/core/domain/gameApiResponse';
import { GameInfo } from 'src/app/core/domain/gameEntity';
import { findPlatformByName } from 'src/app/core/domain/platforms';
import { GameService } from 'src/app/core/services/game-service/game-service';


@Component({

  selector: 'app-select-game',
  templateUrl: './select-game.component.html',
  styleUrls: ['./select-game.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SelectGameComponent), multi: true
  }]
})
export class SelectGameComponent implements OnInit, ControlValueAccessor {

  isLoading = false;
  form: FormGroup = new FormGroup({});
  filtered: GameInfo[] = []

  constructor(private formBuilder: FormBuilder,
    private gameService: GameService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      input: "",
      selectedGame: null,
    })
    this.setOnChanges();
  }

  display(selectedGame: GameInfo) {
    return selectedGame ? selectedGame.name + ` (${selectedGame.releasedYear})` : "";
  }

  displayPlatforms(platforms: string[]): string {
    return platforms.map(p => findPlatformByName(p)?.text)
      .filter(p => p != null).join(', ');
  }

  private setOnChanges() {

    // For typing in input
    this.form.get('input')?.valueChanges.pipe(

      debounceTime(300), // Request only with 300ms of delay
      filter(this.checkIfIsValidInputToSearch), // Only valid inputs (chars count > 3 and is not object)
      tap(() => this.isLoading = true), // change loading to true 
      switchMap(v => from(this.gameService.search(v)).pipe(finalize(() => this.isLoading = false))) // make the request

    ).subscribe((results: GameApiResponse[]) => this.filtered = results.map(convertToGameInfo)); // when the request is finished update results

    // For when select an option
    this.form.get('input')?.valueChanges.pipe(
      filter(this.checkIfIsASelectedvalue)
    ).subscribe(selectedGame => {
      this.form.patchValue({ selectedGame })
      this.onChange(selectedGame);
      this.onTouch(selectedGame);
    })

  }

  private checkIfIsValidInputToSearch(value: string): boolean {
    return typeof value === "string" && value !== "" && value.trim().split('').length > 3
  }

  private checkIfIsASelectedvalue(value: GameInfo): boolean {
    return typeof value === "object" && value.apiId != null;
  }

  writeValue(selectedGame: GameInfo): void {
    this.form.patchValue({ selectedGame });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  onChange = (s : GameInfo) => {}
  onTouch = (s: GameInfo) => {}

}
